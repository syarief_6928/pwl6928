<script type="text/javascript" src="style/js/libs/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="style/js/libs/gumby.min.js"></script>

<script gumby-touch="style/js/libs" src="style/js/libs/gumby.js"></script>
<script src="style/js/libs/ui/gumby.retina.js"></script>
<script src="style/js/libs/ui/gumby.fixed.js"></script>
<script src="style/js/libs/ui/gumby.skiplink.js"></script>
<script src="style/js/libs/ui/gumby.toggleswitch.js"></script>
<script src="style/js/libs/ui/gumby.checkbox.js"></script>
<script src="style/js/libs/ui/gumby.radiobtn.js"></script>
<script src="style/js/libs/ui/gumby.tabs.js"></script>
<script src="style/js/libs/ui/gumby.navbar.js"></script>
<script src="style/js/libs/ui/jquery.validation.js"></script>
<script src="style/js/libs/gumby.init.js"></script>

</body>
</html>