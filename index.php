<?php

// Call Class
include("class/class_db.php");
include("class/class_system.php");

$pdb = new class_db();
$psys = new class_system();

// Get Page
if(isset($_GET["hal"])){ $hal = $psys->anti_injection($_GET["hal"]); }else{ $hal = ""; }

// Content
$hal1 = "halaman/".$hal.".php";

// Header
include("halaman/header.php");

// Content
if(! empty($hal) && file_exists($hal1)){
  include($hal1);
}else{
  include("halaman/home.php");
}

// Footer
include("halaman/footer.php");

?>