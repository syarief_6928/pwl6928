-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Jan 2017 pada 04.14
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tugas_kampus_phototrip`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `halaman`
--

CREATE TABLE IF NOT EXISTS `halaman` (
  `id_halaman` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  PRIMARY KEY (`id_halaman`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9992 ;

--
-- Dumping data untuk tabel `halaman`
--

INSERT INTO `halaman` (`id_halaman`, `judul`, `isi`) VALUES
(9990, 'About Us', '&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin orci metus, iaculis eu metus nec, lobortis aliquam velit. Integer sodales nibh at varius auctor. Nam at leo vel mauris tempor facilisis nec sit amet neque. Integer feugiat dictum egestas. Nam pretium, ligula non molestie egestas, massa tortor auctor massa, ut aliquet ligula mi nec risus. Quisque id eros sit amet neque congue ultricies. Aliquam eget nisi malesuada, faucibus libero sit amet, accumsan libero. Vestibulum non dui dignissim, pretium neque vel, accumsan ligula.&lt;br&gt;&lt;p&gt;&lt;/p&gt;'),
(9991, 'Contact', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin orci metus, iaculis eu metus nec, lobortis aliquam velit. Integer sodales nibh at varius auctor. Nam at leo vel mauris tempor facilisis nec sit amet neque. Integer feugiat dictum egestas. Nam pretium, ligula non molestie egestas, massa tortor auctor massa, ut aliquet ligula mi nec risus. Quisque id eros sit amet neque congue ultricies. Aliquam eget nisi malesuada, faucibus libero sit amet, accumsan libero. Vestibulum non dui dignissim, pretium neque vel, accumsan ligula.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `penanda` text NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama`, `penanda`) VALUES
(2, 'Landscape', 'red-dot-1425477998.png'),
(3, 'Human Interest (HI)', 'red-dot-1425477958.png'),
(4, 'Arsitektur', 'red-dot-1425477994.png'),
(5, 'Prewedding', 'red-dot-1425477986.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `obyek`
--

CREATE TABLE IF NOT EXISTS `obyek` (
  `id_obyek` int(11) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(11) NOT NULL,
  `lat` varchar(50) NOT NULL,
  `lng` varchar(50) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `desk` text NOT NULL,
  `alamat` varchar(225) NOT NULL,
  PRIMARY KEY (`id_obyek`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data untuk tabel `obyek`
--

INSERT INTO `obyek` (`id_obyek`, `id_kategori`, `lat`, `lng`, `judul`, `desk`, `alamat`) VALUES
(1, 4, '-7.809836', '110.359227', 'Tamansari', '', 'Jl. Taman, Kraton, Yogyakarta'),
(2, 2, '-8.016106', '110.310165', 'Gumuk Pasir Parangkusumo', '', 'Jl. Pantai Parangkusumo, Kretek, Bantul, Yogyakarta'),
(4, 3, '-7.798333', '110.365623', 'Pasar Beringharjo', 'Field 1', 'Jl. Ahmad Yani No.16, Yogyakarta'),
(5, 3, '-7.805292', '110.364194', 'Keraton Yogyakarta', '', 'Jl. Rotowijayan 1, Yogyakarta'),
(6, 2, '-8.023037', '110.324583', 'Pantai Parangtritis', '', 'Jl. Parangtritis Km. 28 Parangtritis Yogyakarta'),
(8, 2, '-7.740088', '110.504622', 'Candi Plaosan', '', 'Kokosan, Kecamatan Prambanan, Jawa Tengah'),
(9, 2, '-7.914505', '110.072869', 'Pantai Glagah', '', 'Temon, Kulon Progo, Yogyakarta'),
(10, 2, '-7.602358', '110.178875', ' Bukit Punthuk Setumbu Borobudur', '', 'Desa Karangrejo, Borobudur, Magelang, Jawa Tengah'),
(11, 3, '-7.955543', '110.346531', 'Pasar Wage Pundong', '', 'JL. Pundong , Srihardono , Pundong , Bantul , Daerah Istimewa Yogyakarta'),
(12, 5, '-7.770260', '110.489384', 'Candi Ratu Boko', '', 'Bokoharjo, Sleman, Yogyakarta'),
(13, 2, '-7.751924', '110.491985', 'Candi Prambanan', '', 'Jln. Raya Jogja - Solo Km 16, Prambanan, Sleman, Yogyakarta'),
(14, 2, '-8.154932', '110.621475', 'Pantai Pok Tunggal', '', 'Tepus, Gunungkidul, Yogyakarta'),
(16, 2, '-7.809822', '110.572200', 'Luweng sampang', 'Batuan dengan corak unik ini terbentuk karena erosi dan proses sub-aerial dari air dan material lain yg mengalir di kawasan ini, mengerus bebatuan yg ada sehingga membentuk guratan garis-garis seperti aliran air. Jika musim k', 'Jl. Juminahan, Sampang, Gedangsari, Gunungkidul, D.I Yogyakarta'),
(17, 3, '-7.935882', '110.250056', 'Pembuatan Mie Lethek', 'gasbagag', 'Trimurti, Srandakan, Bantul, Yogyakarta'),
(18, 3, '-7.909460', '110.403642', 'Pembuatan Batik Tulis', '', 'Jl. Karangkulon Giriloyo Wukirsari Imogiri, Bantul, Yogyakarta'),
(19, 3, '-7.844835', '110.335371', 'Pembuatan Gerabah', '', 'Jl. Kasongan, Kasihan, Bantul, Yogyakarta'),
(24, 3, '-8.017878', '110.308139', 'Upacara Budaya Melasti', '', 'Jl. Pantai Parangkusumo, Kretek, Bantul, Yogyakarta'),
(25, 2, '-8.026681', '110.345740', 'Bukit Parang Endong', '', 'Purwosari, Gunung Kidul, Yogyakarta'),
(26, 3, '-7.831575', '110.399381', 'Pembuatan Coklat Monggo Baru', 'ini adalah coklat monggo', 'Jl. Dalem KG III/ 978, Purbayan, Kotagede, Yogyakarta Indonesia'),
(27, 3, '-7.821022', '110.400934', 'Pembuatan Perak', '', 'Jl. kemasan Kotagede, Yogyakarta'),
(28, 2, '-7.855552', '110.535053', 'Curug Kedung Kandang', '', 'Nglanggeran, Patuk, Gunung Kidul, Yogyakarta'),
(29, 2, '-7.847074', '110.547110', 'Embung Nglanggeran', '', ' Nglanggeran, Patuk, Gunung Kidul, Yogyakarta'),
(30, 2, '-7.607862', '110.203746', 'Candi Borobudur', '', 'Borobudur, Magelang, Jawa Tengah'),
(31, 2, '-7.845644', '110.480212', 'Bukit Bintang', '', 'Pathuk, Gunung Kidul, Yogyakarta'),
(32, 2, '-8.150217', '110.612004', 'Pantai Indrayanti', '', 'Tepus, Gunung Kidul, Yogyakarta'),
(33, 2, '-8.180077', '110.676358', 'Pantai Jogan', '', 'Tepus, Gunung Kidul, Yogyakarta'),
(34, 2, '-7.895193', '110.134300', 'Kalibiru', '', 'Hargowilis, Kokap, Kulonprogo, Yogyakarta'),
(35, 2, '-7.825255', '110.122920', 'Waduk Sermo', '', ' Hargowilis, Kokap, Kulon Progo, Yogyakarta'),
(36, 2, '-8.184806', '110.709091', 'Pantai Wediombo', '', 'Jepitu, Girisubo, Gunungkidul, Yogyakarta'),
(37, 2, '-7.747764', '110.626432', 'Rowo Jombor', '', 'Jl. Rawa Jimbung Bayat, Klaten, Jawa Tengah'),
(38, 5, '-7.793389', '110.365672', 'Jalan Malioboro', '', 'Jl. Malioboro, Yogyakarta'),
(39, 5, '-7.800210', '110.366253', 'Benteng Vredeburg', '', 'Jl. Jenderal A. Yani No. 6 Yogyakarta'),
(40, 5, '-7.801368', '110.364803', '0 KM Yogyakarta', '', 'Jl. Margo Mulyo Gondomanan, Yogyakarta'),
(41, 3, '-7.804059', '110.364395', 'Upacara Grebeg', '', 'Jl. Alun-alun Utara, Yogyakarta'),
(42, 3, '-7.794847', '110.326052', 'Upacara Saparan Bekakak', 'Upacara adat Saparan Bekakak adalah asgasgasgaghahahadhdsjsadhjahahahah aahahafhahahadfhadhadhahaha hadfhahahaahahahadfhahahahad ffffffffffddddddddhadhahadfhd hfhadfhhhhhhhhadhahahaha hahahaahahhhhhhhh hhhhhhharga', 'Ambarketawang, Gamping, Yogyakarta'),
(43, 3, '-7.806253', '110.361890', 'Upacara Jamasan Pusaka', '', 'Jl. Rotowijayan Kraton, Yogyakarta'),
(45, 2, '-8.145315', '110.599045', 'Pantai Krakal', 'pantai asgasgasgasgajgkajgaksgas&#039;glasgjas;gggjg;asgjasgkj fsagjasgjasgkajs&#039;gkas&#039;gasga&#039;as;glasgjas;glas;gj;lsjg;sgs;gjs;g;sg;sg;sgj;asga;lga;ll;a;aNol Kilometer (0 KM) Jogja ini adalah salah satu tempat fav', 'Jl. Pantai Krakal, Ngestirejo, Yogyakarta'),
(46, 3, '-7.945541', '110.426695', 'Jembatan Gantung Selopamioro', '', 'Selopamioro, Imogiri, Bantul, Yogyakarta'),
(47, 3, '-7.906892', '110.414291', 'Upacara Nguras Enceh', '', 'Imogiri, Bantul, Yogyakarta'),
(48, 3, '-7.768764', '110.254239', 'Pembuatan Keris', 'ini adalah pembuatan keris di jogjakarta', 'Gatak, Sumberagung, Moyudan, Sleman, Yogyakarta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `obyek_gambar`
--

CREATE TABLE IF NOT EXISTS `obyek_gambar` (
  `id_obyek_gambar` int(11) NOT NULL AUTO_INCREMENT,
  `id_obyek` int(11) NOT NULL,
  `gambar` text NOT NULL,
  PRIMARY KEY (`id_obyek_gambar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

--
-- Dumping data untuk tabel `obyek_gambar`
--

INSERT INTO `obyek_gambar` (`id_obyek_gambar`, `id_obyek`, `gambar`) VALUES
(13, 1, 'images-1421488019.png'),
(14, 8, 'images-1421527644.png'),
(15, 10, 'images-1421530779.png'),
(16, 11, 'images-1421803428.png'),
(17, 14, '91928f6f5e5a5435bdf725213dd7823d-1424747529.jpg'),
(23, 33, 'pantai-jogan-1426230947.jpg'),
(34, 42, '11831_1196669431582_2657802_n-1426411992.jpg'),
(35, 42, 'boneka-bekakak-1426412005.jpg'),
(36, 42, 'f1df4bb184a3736518a6286315a96005-1426412015.jpg'),
(37, 42, 'j06122014130524-1426412023.jpg'),
(38, 40, 'pre-wedding-photographer-jogja-for-dashant-tanusha-1426445046.jpg'),
(39, 40, 'lokasi-foto-pre-wedding-outdoor-favorit-di-jogja-1426445058.jpg'),
(40, 40, 'img_0721-1426445075.jpg'),
(41, 40, 'foto-7-1426445084.jpg'),
(42, 25, '2-1426445271.jpg'),
(43, 25, '1916431-atlet-paralayang-di-gunung-kidul-620x310-1426445275.gif'),
(44, 25, '4611038_tendemmalaysia-1426445278.jpg'),
(45, 25, 'images-1426445283.jpg'),
(46, 18, '382593_10201415838706694_488957167_n-1426445420.jpg'),
(47, 18, '522105_10205507727081960_8878000865589177240_n-1426445423.jpg'),
(48, 18, '542900_4689551930341_53709940_n-1426445426.jpg'),
(49, 18, '10846450_10205461022194367_959843958742317326_n-1426445432.jpg'),
(50, 30, '4171979698_f5870e49a6_b-1426445533.jpg'),
(51, 30, 'images-(1)-1426445538.jpg'),
(52, 30, 'images-1426445542.jpg'),
(53, 30, 'sunrise-candi-borobudur-750x498-1426445547.jpg'),
(54, 43, '1-1427445344.jpg'),
(55, 43, '1suro1-1427445348.jpg'),
(56, 43, '6-1427445351.jpg'),
(57, 43, '3194393986-1427445355.jpg'),
(58, 41, '6708101-1427445959.jpg'),
(59, 41, 'grebeg1-1427445963.jpg'),
(60, 41, 'grebeg-maulud-1433-h-1427445967.jpg'),
(61, 41, 'grebek-syawal13-1427445970.jpg'),
(62, 41, 'jogjaicon.blogspot-1427445974.jpg'),
(63, 47, 'foto-nguras-enceh-_-2-keraton-kerja-bareng-kuras-enceh-1427448199.jpg'),
(64, 47, 'nguras-enceh1-1427448203.jpg'),
(65, 47, 'prosesi-nguras-enceh-071114-nov-2-1427448208.jpg'),
(66, 47, 'img_1270(fileminimizer)-1427448212.jpg'),
(67, 47, '1e47a0aa3a2d20cd8c14bfe3f00b5f14-1427448213.jpg'),
(68, 24, '13465798263_3cd867c9cb_o-1427452531.jpg'),
(69, 24, 'antarafoto-upacara-melasti-afa-1427452533.jpg'),
(70, 24, 'antarafoto-upacara-melasti-yogyakarta-150315-sgd-9-1427452539.jpg'),
(71, 24, 'melasti1-1427452540.jpg'),
(72, 24, 'melasti004-1427452558.jpg'),
(73, 48, 'edit-1427482997.jpg'),
(74, 48, '804_diy_proses_pemanasan_pembuatan_keris_-_ie-1427483003.jpg'),
(75, 48, 'copy-of-proses-pembuatan-solo2-1427483010.jpg'),
(76, 48, 'menempa-baja-pembuatan-keris_full-1427483015.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `uname` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama`, `uname`, `pwd`) VALUES
(1, 'Administrator', 'photo', 'trip'),
(2, 'Administrator 2', 'admin', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
