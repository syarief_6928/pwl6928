<script type="text/javascript" src="style/js/jquery.remodal.js"></script>

<script type="text/javascript">
	
	$(document).on('cancel', '.remodal', function () {
        console.log('cancel');
    });

    $(document).on('close', '.remodal', function () {
        console.log('close');
    });

    var inst = $('[data-remodal-id=modal]').remodal();

    function page(id){  	
    	$.ajax({
			type: 	"GET",
			url: 	"mod/function.php?type=page&id="+id,
			success: function(result){
				$("#modal_hangar_detail").remove();
				$("#modal_hangar").append(result);
				inst.open();
			}
		});
    }

    /*
    $("#slideshow > div:gt(0)").hide();

	setInterval(function() { 
	  $('#slideshow > div:first')
	    .fadeOut(1000)
	    .next()
	    .fadeIn(1000)
	    .end()
	    .appendTo('#slideshow');
	},  3000);
*/
    
    $(function(){
	    $('.fadein img:gt(0)').hide();
	    setInterval(function(){
	      $('.fadein :first-child').fadeOut()
	         .next('img').fadeIn()
	         .end().appendTo('.fadein');}, 
	      5000);
	});
	

</script>
</body>
</html>