<script type="text/javascript">
	
	function login(){

		var uname		= $("#uname").val();
		var pwd 	 	= $("#pwd").val();

		$.ajax({
			type: 	"GET",
			url: 	"mod/function.php?type=login&uname=" + uname + "&pwd=" + pwd,
			success : function(result){
				
				var result = eval('('+result+')');
				if(result.sukses){
					location='admin/';
				}else{
					alert('gagal');
				}

			}
		});

	}

</script>

<div id="wrapper">

	<form name="login-form" class="login-form" action="" method="post">
	
		<div class="header">
		<h1>Login Form</h1>
		<span>Gunakan username dan password dengan benar untuk masuk kehalaman administrator.</span>
		</div>
	
		<div class="content">
		<input name="username" type="text" class="input username" placeholder="Username" id="uname" name="uname" />
		<div class="user-icon"></div>
		<input name="password" type="password" class="input password" placeholder="Password" id="pwd" name="pwd" onkeydown="if(event.keyCode == 13){ login() }" />
		<div class="pass-icon"></div>		
		</div>

		<div class="footer">
			<div class="button" onclick="login()">login</div>
		
		</div>
	
	</form>

</div>
<div class="gradient"></div>

