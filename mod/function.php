
<?php 

// Call Class
include("../class/class_db.php");
include("../class/class_system.php");

$pdb = new class_db();
$psys = new class_system();

if(isset($_GET["type"])){ $type = $psys->anti_injection($_GET["type"]); }else{ $type = ""; }

if($type == "login"){
	
	if(isset($_GET["uname"])){ $uname = $psys->anti_injection($_GET["uname"]); }else{ $uname = ""; }
	if(isset($_GET["pwd"])){ $pwd = $psys->anti_injection($_GET["pwd"]); }else{ $pwd = ""; }

	$sql = $pdb->Query("*", "user", "uname = '$uname' AND pwd = '$pwd'", "", "one");

	if($sql){
		$_SESSION["user"] = $sql["id_user"];
		echo json_encode(array('sukses' => true));
	}else{
		echo json_encode(array('salah' => true));
	}

}elseif($type == "page"){

	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }

	$sql = $pdb->Query("*", "halaman", "id_halaman = '$id'", "", "one");

	$judul = $sql["judul"];
	$isi = html_entity_decode($sql["isi"]);

	$content = "<div id='modal_hangar_detail'><h1>".$judul."</h1><div class='remodal_isi'>".$isi."</div></div>";

	echo $content;

}elseif($type == "image_slide"){

	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }

	$content = '<ul class="slides">';

	$i = 1;
	$sql = $pdb->Query("*", "obyek_gambar", "id_obyek = '$id'", "", "all");
	$count = $pdb->Query("*", "obyek_gambar", "id_obyek = '$id'", "", "much");
	while($row = mysql_fetch_array($sql)){
		$gambar = $row["gambar"];

		$content .= '<input type="radio" name="radio-btn" id="img-'.$i.'" checked />
					    <li class="slide-container">
							<div class="slide">
								<img src="../gambar/obyek/'.$gambar.'" />
					        </div>
							<div class="nav">';

		if($i == 1){
			$content .=	'<label for="img-'.$count.'" class="prev">&#x2039;</label>';
		}else{
			$content .=	'<label for="img-'.$i--.'" class="prev">&#x2039;</label>';
		}

		if($i == $count){
			$content .= '<label for="img-1" class="next">&#x203a;</label>';
		}else{
			$content .= '<label for="img-'.$i++.'" class="next">&#x203a;</label>';
		}

		$content .= '</div></li>';
							
	}

	$content .= '</ul>';

	echo $content;

}elseif($type == "save_halaman"){

	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }
	
	if(isset($_GET["judul"])){ $judul = $psys->anti_injection($_GET["judul"]); }else{ $judul = ""; }
	if(isset($_GET["isi"])){ $isi = $psys->anti_injection($_GET["isi"]); }else{ $isi = ""; }
	
	$isi_db = array('judul' => $judul, 'isi' => $isi);
	$sql = $pdb->Update($isi_db, "halaman", "id_halaman = '$id'");

	if($sql){
		echo json_encode(array('sukses' => true));
	}else{
		echo json_encode(array('salah' => true));
	}

}elseif($type == "save_obyek"){

	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }
	
	if(isset($_GET["id_kategori"])){ $id_kategori = $psys->anti_injection($_GET["id_kategori"]); }else{ $id_kategori = ""; }
	if(isset($_GET["judul"])){ $judul = $psys->anti_injection($_GET["judul"]); }else{ $judul = ""; }
	if(isset($_GET["lat"])){ $lat = $psys->anti_injection($_GET["lat"]); }else{ $lat = ""; }
	if(isset($_GET["lng"])){ $lng = $psys->anti_injection($_GET["lng"]); }else{ $lng = ""; }
	if(isset($_GET["desk"])){ $desk = $psys->anti_injection($_GET["desk"]); }else{ $desk = ""; }
	if(isset($_GET["alamat"])){ $alamat = $psys->anti_injection($_GET["alamat"]); }else{ $alamat = ""; }
	// if(isset($_GET["tips"])){ $tips = $psys->anti_injection($_GET["tips"]); }else{ $tips = ""; }

	if($id != ""){
		$isi_db = array('id_kategori' => $id_kategori, 'judul' => $judul, 'lat' => $lat, 'lng' => $lng, 'desk' => $desk, 'alamat' => $alamat);
		$sql = $pdb->Update($isi_db, "obyek", "id_obyek = '$id'");
	}else{
		$isi_db = array('id_kategori' => $id_kategori, 'judul' => $judul, 'lat' => $lat, 'lng' => $lng, 'desk' => $desk, 'alamat' => $alamat);
		$sql = $pdb->Insert($isi_db, "obyek");
	}

	if($sql){
		echo json_encode(array('sukses' => true));
	}else{
		echo json_encode(array('salah' => true));
	}

}elseif($type == "save_kategori"){

	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }
	if(isset($_GET["nama"])){ $nama = $psys->anti_injection($_GET["nama"]); }else{ $nama = ""; }

	if($id != ""){
		$isi_db = array('nama' => $nama);
		$sql = $pdb->Update($isi_db, "kategori", "id_kategori = '$id'");
	}else{
		$isi_db = array('nama' => $nama);
		$sql = $pdb->Insert($isi_db, "kategori");
	}

	if($sql){
		echo json_encode(array('sukses' => true));
	}else{
		echo json_encode(array('salah' => true));
	}

}elseif($type == "save_user"){

	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }
	if(isset($_GET["nama"])){ $nama = $psys->anti_injection($_GET["nama"]); }else{ $nama = ""; }
	if(isset($_GET["uname"])){ $uname = $psys->anti_injection($_GET["uname"]); }else{ $uname = ""; }
	if(isset($_GET["pwd"])){ $pwd = $psys->anti_injection($_GET["pwd"]); }else{ $pwd = ""; }

	if($id != ""){
		$isi_db = array('nama' => $nama, 'uname' => $uname, 'pwd' => $pwd);
		$sql = $pdb->Update($isi_db, "user", "id_user = '$id'");
	}else{
		$isi_db = array('nama' => $nama, 'uname' => $uname, 'pwd' => $pwd);
		$sql = $pdb->Insert($isi_db, "user");
	}

	if($sql){
		echo json_encode(array('sukses' => true));
	}else{
		echo json_encode(array('salah' => true));
	}	

}elseif($type == "del_obyek"){

	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }
	
	$cek = $pdb->Query("id_obyek_gambar, gambar", "obyek_gambar", "id_obyek = '$id'", "", "all");
	while($dapat = mysql_fetch_array($cek)){
		$dapat_id_gbr = $dapat["id_obyek_gambar"];
		$sql_del = $pdb->Delete("obyek_gambar", "id_obyek_gambar = '$dapat_id_gbr'");

		$dapat_gbr = $dapat["gambar"];
		unlink("../../gambar/obyek/".$dapat_gbr);
	}

	$sql = $pdb->Delete("obyek", "id_obyek = '$id'");
	if($sql){
		echo json_encode(array('sukses' => true));
	}else{
		echo json_encode(array('salah' => true));
	}		

}elseif($type == "gambar_obyek"){
	
	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }
	
	$html = "<div id='gbr_here'>";
	$sql = $pdb->Query("*", "obyek_gambar", "id_obyek = '$id'", "", "all");
	if($sql){
		while($baris = mysql_fetch_array($sql)){
			$gbr = $baris["gambar"];
			$gbr_js = $baris["id_obyek_gambar"];
			$html .= "<img src='../gambar/obyek/".$gbr."' width='150' onclick='hapusGambar(".$gbr_js.")' id='gbr_".$gbr_js."' style='cursor:pointer;'>";
		}
	}	

	$html .= "</div>";	
	echo $html;

}elseif($type == "gambar_obyek_hapus"){
	
	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }
	
	$dapat= $pdb->Query("gambar", "obyek_gambar", "id_obyek_gambar = '$id'", "", "one");
	$gambar = $dapat["gambar"];

	$sql_del = $pdb->Delete("obyek_gambar", "id_obyek_gambar = '$id'");
	if($sql){
		unlink("../../gambar/obyek/".$gambar);
		echo json_encode(array('sukses' => true));
	}else{
		echo json_encode(array('salah' => true));
	}	

}elseif($type == "gambar_kategori"){
	
	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }
	
	$html = "<div id='gbr_here'>";
	$sql= $pdb->Query("penanda", "kategori", "id_kategori = '$id'", "", "one");
	$hitung = $pdb->Query("penanda", "kategori", "id_kategori = '$id'", "", "much");
	if($hitung > 0){
		$gbr = $sql["penanda"];
		if($gbr != NULL || $gbr != ""){
			$html .= "<img src='../gambar/icon/".$gbr."' onclick='hapusGambar(".$id.")' style='cursor:pointer;' width='150' id='gbr_".$id."'>";
		}
		
	}	

	$html .= "</div>";	
	echo $html;	

}elseif($type == "gambar_kategori_hapus"){
	
	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }
	
	$dapat= $pdb->Query("penanda", "kategori", "id_kategori = '$id'", "", "one");
	$gambar = $dapat["penanda"];
	
	$isi_db = array('penanda' => "");
	$sql = $pdb->Update($isi_db, "kategori", "id_kategori = '$id'");
	if($sql){
		unlink("../../gambar/icon/".$gambar);
		echo json_encode(array('sukses' => true));
	}else{
		echo json_encode(array('salah' => true));
	}	

}elseif($type == "del_kategori"){

	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }
	
	$dapat= $pdb->Query("penanda", "kategori", "id_kategori = '$id'", "", "one");
	$dapat_gbr = $dapat["penanda"];

	$sql = $pdb->Delete("kategori", "id_kategori = '$id'");
	if($sql){
		if($dapat_gbr != ""){
			unlink("../../gambar/icon/".$dapat_gbr);
		}
		echo json_encode(array('sukses' => true));
	}else{
		echo json_encode(array('salah' => true));
	}	

}elseif($type == "del_user"){

	if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }
	
	

	$sql = $pdb->Delete("user", "id_user = '$id'");
	if($sql){
		echo json_encode(array('sukses' => true));
	}else{
		echo json_encode(array('salah' => true));
	}		

}else{
	// Do Nothing
}	

?>