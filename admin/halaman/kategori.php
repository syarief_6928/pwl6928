<div class="row">
	<div class="twelve columns right">
		<div class="medium primary btn icon-right entypo icon-plus-squared"><a href=".?hal=kategori_form">Tambah Kategori Baru</a></div>
	</div>
</div>

<div class="row">&nbsp;</div>

<div class="row">
	<div class="twelve columns">
      <table class="rounded table_small">
		<thead>
			<tr>
				<th width="90%">Kategori</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$sql = $pdb->Query("*", "kategori", "", "id_kategori DESC", "all");
			while($baris = mysql_fetch_array($sql)){
				$id = $baris["id_kategori"];
			?>
			<tr>
				<td onclick="location = '?hal=kategori_form&id=<?php echo $id; ?>'"><?php echo $baris["nama"]; ?></td>
				<td><!-- <a style="cursor:pointer;" onclick="gbr(<?php echo $id; ?>)"><i class="icon-picture"></i></a> --></td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
    </div>
</div>

<div class="modal" id="detail-gambar">
  <div class="content">
	<a class="close switch" gumby-trigger="|#detail-gambar"><i class="icon-cancel" /></i></a>		
				
				<div id="detail_gambar_place" class="info-table barang-inmodal">
					<input type="hidden" id="modal_id" name="modal_id">
					<div id="gbr_place"></div>
					<div id="mulitplefileuploader">Upload</div>
					<div id="mulitplefileuploader_status"></div>
				</div>

  </div>
</div>

<script type="text/javascript">
	
function imgUpload(){
	var modalId = $("#modal_id").val();
	var settings = {
		url: "halaman/upload_image.php?type=gambar_kategori&id="+modalId,
		method: "POST",
		allowedTypes:"jpg,png,gif,jpeg",
		fileName: "myfile",
		multiple: false,
		onSuccess:function(files,data,xhr)
		{
			$("#mulitplefileuploader_status").html("<font color='green'>Upload is success</font>");
			
			
		},
		onError: function(files,status,errMsg)
		{		
			$("#mulitplefileuploader_status").html("<font color='red'>Upload is Failed</font>");
		}
	}
	$("#mulitplefileuploader").uploadFile(settings);
}

function gbr(id){
	$.ajax({
		type: 	"GET",
		url: 	"../mod/function.php?type=gambar_kategori&id=" + id,
		success : function(result){
			
			$("#gbr_here").remove();
			$(".ajax-upload-dragdrop").remove();
			$(".upload-statusbar").remove();
			$("#mulitplefileuploader_status").remove();

			$("#modal_id").val(id);
			$("#gbr_place").append(result);
			imgUpload();
			$('#detail-gambar').addClass('active');

		}
	});
}

function hapusGambar(id){
	
	if(confirm('Anda yakin ingin menghapus data ini?')){
		$.ajax({
			type: 	"GET",	
			url: 	"../mod/function.php?type=gambar_kategori_hapus&id=" + id,
			success : function(result){
				
				var result = eval('('+result+')');
				if(result.sukses){
					$("#gbr_"+id).remove();
				}else{
					alert("Maaf, Ada kesalahan data... mohon dicek kembali")
				}

			}
		});
	}else{
		// Do nothind
	}

}

</script>