<?php

// Call Class
include("../class/class_db.php");
include("../class/class_system.php");

$pdb = new class_db();
$psys = new class_system();

if(isset($_GET["type"])){ $type = $psys->anti_injection($_GET["type"]); }else{ $type = ""; }

function getExtension($str) {
     $i = strrpos($str,".");
     if (!$i) { return ""; }
     $l = strlen($str) - $i;
     $ext = substr($str,$i+1,$l);
     return $ext;
}

$output_dir = "../../gambar/obyek/";


if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }

if(isset($_FILES["myfile"]))
{
	$ret = array();

	$error =$_FILES["myfile"]["error"];
   {
    
    	if(!is_array($_FILES["myfile"]['name'])) //single file
    	{
            $RandomNum   = time();

            $ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name']));
            $ImageType      = $_FILES['myfile']['type']; //"image/png", image/jpeg etc.
         
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt       = str_replace('.','',$ImageExt);
            $ImageName      = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $ImageName.'-'.$RandomNum.'.'.$ImageExt;

            // Custom Start
            $temp_custom = $_FILES["myfile"]["tmp_name"];

            switch(strtolower($ImageType))
            {
                case 'image/png':
                    $CreatedImage = imagecreatefrompng($temp_custom);
                    break;
                case 'image/gif':
                    $CreatedImage = imagecreatefromgif($temp_custom);
                    break;
                case 'image/jpeg':
                case 'image/pjpeg':
                    $CreatedImage = imagecreatefromjpeg($temp_custom);
                    break;
                default:
                    $processImage = false; //image format is not supported!
            }
            /*
            $ThumbSquareSize = 600;
            $thumb_DestRandImageName = $output_dir.$NewImageName;
            $Quality = 90;
            */
            list($CurWidth,$CurHeight)=getimagesize($temp_custom);


            move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $NewImageName);

            // Insert into DB
            $isi_db = array('id_obyek' => $id, 'gambar' => $NewImageName);
            $sql = $pdb->Insert($isi_db, "obyek_gambar");
  
	       	$ret[$fileName]= $output_dir.$NewImageName;

             
    	}
        else
        {
            $fileCount = count($_FILES["myfile"]['name']);
            for($i=0; $i < $fileCount; $i++)
            {
                $RandomNum   = time();
            
                $ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name'][$i]));
                $ImageType      = $_FILES['myfile']['type'][$i]; //"image/png", image/jpeg etc.
             
                $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
                $ImageExt       = str_replace('.','',$ImageExt);
                $ImageName      = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
                $NewImageName = $ImageName.'-'.$RandomNum.'.'.$ImageExt;

                 // Custom Start
                $temp_custom = $_FILES["myfile"]["tmp_name"][$i];

                switch(strtolower($ImageType))
                {
                    case 'image/png':
                        $CreatedImage = imagecreatefrompng($temp_custom);
                        break;
                    case 'image/gif':
                        $CreatedImage = imagecreatefromgif($temp_custom);
                        break;
                    case 'image/jpeg':
                    case 'image/pjpeg':
                        $CreatedImage = imagecreatefromjpeg($temp_custom);
                        break;
                    default:
                        $processImage = false; //image format is not supported!
                }

                /*
                $ThumbSquareSize = 600;
                $thumb_DestRandImageName = $output_dir.$NewImageName;
                $Quality = 90;
                */

                list($CurWidth,$CurHeight)=getimagesize($temp_custom);

                move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$NewImageName );
            
                // Insert into DB
                $isi_db = array('id_obyek' => $id, 'gambar' => $NewImageName);
                $sql = $pdb->Insert($isi_db, "obyek_gambar");

                $ret[$NewImageName]= $output_dir.$NewImageName;
           
            }
        }
    }
    echo json_encode($ret);
 
}


?>