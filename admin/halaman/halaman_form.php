<script type="text/javascript">
$(document).ready(
	function()
	{
		$('#isi').redactor({
			imageUpload: 'halaman/redactor_image_upload.php'
		});
	}
);
</script>

<?php 
if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }

$baris = $pdb->Query("*", "halaman", "id_halaman = '$id'", "", "one");
?>

<div class="row">
	<div class="twelve columns">
		
		<input type="hidden" name="id" id="id" value="<?php echo $id; ?>">

		<div class="field" id="fjudul">
		  <input class="input" type="text" value="<?php echo $baris['judul']; ?>" id="judul" name"judul" />
		</div>
		<div class="field" id="fisi">
			<textarea class="xxwide text input textarea" placeholder="Isi" id="isi" name="isi"><?php echo $baris['isi']; ?></textarea>
		</div>
		

	</div>
</div>

<div class="row">
	<div class="twelve columns right">
		<div class="medium primary btn icon-right entypo icon-check" style="cursor:pointer;" onclick="saveHalaman()"><a>Simpan Data</a></div>
	</div>
</div>

<div class="row">&nbsp;</div>

<script type="text/javascript">
	
	function saveHalaman(){

		var id	= $("#id").val();

		var judul	= $("#judul").val();
		var isi 	= $("#isi").val();

		if(judul != ""){ $('#fjudul').removeClass('warning'); }
		if(isi != ""){ $('#fisi').removeClass('warning'); }

		if(judul == ""){ $('#fjudul').addClass('warning');
		}else if(isi == ""){ $('#fisi').addClass('warning');
		}else{

			var kiriman = "&id="+id+"&judul="+judul+"&isi="+isi;	

			$.ajax({
				type: 	"GET",
				url: 	"../mod/function.php?type=save_halaman" + kiriman,
				success : function(result){
					
					var result = eval('('+result+')');
					if(result.sukses){
						location = ".?hal=halaman";
					}else{
						alert("Maaf, ada kesalahan dalam pemrosesan data");
					}

				}
			});

		}
	}

</script>