<div class="row">
	<div class="twelve columns right">
		<div class="medium primary btn icon-right entypo icon-list"><a href=".?hal=kategori">Kategori</a></div>
		<div class="medium primary btn icon-right entypo icon-plus-squared"><a href=".?hal=obyek_form">Tambah Obyek Baru</a></div>
	</div>
</div>

<div class="row">&nbsp;</div>

<div class="row">
	<div class="twelve columns">
      <table class="rounded table_small">
		<thead>
			<tr>
				<th>Judul</th>
				<th>Lat, Lng</th>
				<th>Kategori</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$sql = $pdb->Query("*", "obyek INNER JOIN kategori ON obyek.id_kategori = kategori.id_kategori", "", "id_obyek DESC", "all");
			while($baris = mysql_fetch_array($sql)){
				$id = $baris["id_obyek"];
			?>
			<tr>
				<td onclick="location = '?hal=obyek_form&id=<?php echo $id; ?>'"><?php echo $baris["judul"]; ?></td>
				<td onclick="location = '?hal=obyek_form&id=<?php echo $id; ?>'"><?php echo $baris["lat"].", ".$baris["lng"]; ?></td>
				<td onclick="location = '?hal=obyek_form&id=<?php echo $id; ?>'"><?php echo $baris["nama"]; ?></td>
				<td><a style="cursor:pointer;" onclick="gbrMultiple(<?php echo $id; ?>)"><i class="icon-picture"></i></a></td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
    </div>
</div>

<div class="modal" id="detail-gambar">
  <div class="content">
	<a class="close switch" gumby-trigger="|#detail-gambar"><i class="icon-cancel" /></i></a>		
				
				<div id="detail_gambar_place" class="info-table barang-inmodal">
					<input type="hidden" id="modal_id" name="modal_id">
					<div id="gbr_place"></div>
					<div id="mulitplefileuploader">Upload</div>
					<div id="mulitplefileuploader_status"></div>
				</div>

  </div>
</div>

<script type="text/javascript">
	
function imgUpload(){
	var modalId = $("#modal_id").val();
	var settings = {
		url: "halaman/upload_image_multiple.php?type=gambar_obyek&id="+modalId,
		method: "POST",
		allowedTypes:"jpg,png,gif,jpeg",
		fileName: "myfile",
		multiple: true,
		onSuccess:function(files,data,xhr)
		{
			$("#mulitplefileuploader_status").html("<font color='green'>Upload is success</font>");
			
			
		},
		onError: function(files,status,errMsg)
		{		
			$("#mulitplefileuploader_status").html("<font color='red'>Upload is Failed</font>");
		}
	}
	$("#mulitplefileuploader").uploadFile(settings);
}

function gbrMultiple(id){
	$.ajax({
		type: 	"GET",
		url: 	"../mod/function.php?type=gambar_obyek&id=" + id,
		success : function(result){
			
			$("#gbr_here").remove();
			$(".ajax-upload-dragdrop").remove();
			$(".upload-statusbar").remove();
			$("#mulitplefileuploader_status").remove();

			$("#modal_id").val(id);
			$("#gbr_place").append(result);
			imgUpload();
			$('#detail-gambar').addClass('active');

		}
	});
}

function hapusGambar(id){
	
	if(confirm('Anda yakin ingin menghapus data ini?')){
		$.ajax({
			type: 	"GET",	
			url: 	"../mod/function.php?type=gambar_obyek_hapus&id=" + id,
			success : function(result){
				
				var result = eval('('+result+')');
				if(result.sukses){
					$("#gbr_"+id).remove();
				}else{
					alert("Maaf, Ada kesalahan data... mohon dicek kembali")
				}

			}
		});
	}else{
		// Do nothind
	}

}

</script>