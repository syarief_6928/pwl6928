<?php

class class_system {

	public function __construct(){
		session_start();
	}

	public function harga($no){
		$rp = number_format($no, 2, ',', '.');
		$output = "Rp. ".$rp;
		return $output;
	}

	public function anti_injection($value){
		$sent_back = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($value,ENT_QUOTES))));
		return $sent_back;
	}

	public function cek_login(){
		if(! isset($_SESSION["user"])){
			echo "<script>location='../?hal=login';</script>";
		}
	}

}

?>