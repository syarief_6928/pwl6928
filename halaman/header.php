<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	
<title>PhotoTrip - Sistem Informasi Geografis Untuk Pemetaan Lokasi Spot Foto di Yogyakarta</title>
	
    <?php if($hal == "login"){ echo '<link rel="stylesheet" type="text/css" href="style/css/login.css">'; } ?>
    <link rel="stylesheet" type="text/css" href="style/css/style.css">
    <link rel="stylesheet" type="text/css" href="style/css/jquery.remodal.css">

    <script type="text/javascript" src="style/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="style/js/infobubble.js"></script>
    

    <?php if(empty($hal) && !file_exists($hal1)){ ?>
  
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALO8La--5KRBKKw7vVscuWGk390vGueXU&sensor=TRUE&language=id"></script>

    <script type="text/javascript">
    var jogja = new google.maps.LatLng(-7.8032505, 110.374845); // First Center LatLng
    var map;

    var imgG = "";
    var posisiKu = "";

    var directionsDisplay = new google.maps.DirectionsRenderer(/*{suppressMarkers: true}*/);

    // Var. Def Marker
    var kingMarkers = []; 
    var listMarkers = [
        <?php
        $list_i = 1;

        $sql_field  = "id_obyek, judul, lat, lng, desk, alamat, obyek.id_kategori as id_kategori, penanda";
        $list_sql   = $pdb->Query($sql_field, "obyek INNER JOIN kategori ON obyek.id_kategori = kategori.id_kategori", "", "", "all");
        $list_hitung= $pdb->Query($sql_field, "obyek INNER JOIN kategori ON obyek.id_kategori = kategori.id_kategori", "", "", "much");

        while($list_baris = mysql_fetch_array($list_sql)){
          $judul = $list_baris["judul"];
          $lat = $list_baris["lat"];
          $lng = $list_baris["lng"];
          $desk = $list_baris["desk"];
          $alamat = $list_baris["alamat"];
          $id_kategori = $list_baris["id_kategori"];
          $penanda = $list_baris["penanda"];
          $id_obyek = $list_baris["id_obyek"];
         

          echo "['".$judul."', ".$lat.", ".$lng.", '".$alamat."', ".$id_kategori.", '".$penanda."', '".$desk."', '".$id_obyek."']";

          if($list_i < $list_hitung){
            echo ", ";
          }
          $list_i++;
        }
        ?>
      ];

      var listImages = [
        <?php
        $image_i = 1;
        $image_sql   = $pdb->Query("id_obyek_gambar, id_obyek, gambar", "obyek_gambar", "", "id_obyek ASC", "all");
        $image_hitung = $pdb->Query("id_obyek_gambar, id_obyek, gambar", "obyek_gambar", "", "id_obyek ASC", "much");

        while($image_baris = mysql_fetch_array($image_sql)){
          $id_obyek_gambar = $image_baris["id_obyek_gambar"];
          $id_obyek = $image_baris["id_obyek"];
          $gambar = $image_baris["gambar"];

          echo "['".$id_obyek_gambar."', ".$id_obyek.", '".$gambar."']";

          if($image_i < $image_hitung){
            echo ", ";
          }
          $image_i++;
        }
        ?>
      ];

     
    function initialize() {
      
      var mapOptions = {
        center: jogja,
        zoom: 13,
        panControl: false,
        zoomControl: true,
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP
        },
        mapTypeControl: true,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false
      };

      map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
      

      createMarker(map, listMarkers, listImages);   // Create All Marker
      show(0);  // Show All Marker

      // Find me button
      var findMeDiv = document.createElement('div');
      var findMebtn = new findMe(findMeDiv, map);

      findMeDiv.index = 1;
      map.controls[google.maps.ControlPosition.TOP_RIGHT].push(findMeDiv);

      // Clear maps button
      var clearMapsDiv = document.createElement('div');
      var clearMapsbtn = new clearMaps(clearMapsDiv, map);

      clearMapsDiv.index = 1;
      map.controls[google.maps.ControlPosition.TOP_RIGHT].push(clearMapsDiv);
      
    }  
    
    function createMarker(map, list, images) {  

       var infoWindow = new google.maps.InfoWindow();

       var infoBubble = new InfoBubble({
          padding: 10,
          borderRadius: 4,
          arrowSize: 0,
          arrowPosition: 30,
          arrowStyle: 2,
          maxWidth: 300,
          maxHeight: 300
      });

       for (var i = 0; i < list.length; i++) {
          var newMarker = list[i];
          var myLatLng = new google.maps.LatLng(newMarker[1], newMarker[2]);
          var geoLoc = "<div class='find_geo' onclick='findGeolocation("+newMarker[1]+", "+newMarker[2]+")'>Get Direction</div>";

          var image = {
            url: "gambar/icon/"+newMarker[5],
            size: new google.maps.Size(32, 37),
            origin: new google.maps.Point(0,0),
            anchor: new google.maps.Point(0, 32)
          };

          var marker = new google.maps.Marker({
              position: myLatLng,
              map: map,
              animation: google.maps.Animation.DROP,
              title: newMarker[0]
          });
         
          // Image Slideshow Start
          var getImg = '<div class="fadein">';
          var hitImg = 1;
          for (var j = 0; j < images.length; j++){
            var newImage = images[j];

            if(newImage[1] == newMarker[7]){

              getImg = getImg + '<img src="gambar/obyek/'+newImage[2]+'">';  

              hitImg++;

            }
          }
          getImg = getImg + '</div>';
          // Image Slideshow Stop

          

          var contentInfo = "<h3>"+newMarker[0]+"</h3><div class='desk'>"+newMarker[6]+"</div><div class='get_direct_button'>"+geoLoc+"</div>";
          var contentFoto = getImg;
          var contentAlamat = "<div class='desk'>"+newMarker[3]+"</div>";

          var contentId = newMarker[4];
          bindInfoWindow(marker, map, infoBubble, contentInfo, contentFoto, contentAlamat, contentId); 
        }


    }

    function bindInfoWindow(marker, map, infoBubble, contentInfo, contentFoto, contentAlamat, contentId){
       google.maps.event.addListener(marker, 'click', (function(){
            infoBubble.close();
            infoBubble.removeTab(2);
            infoBubble.removeTab(1);
            infoBubble.removeTab(0); 

            infoBubble.addTab('Info', contentInfo);
            infoBubble.addTab('Foto', contentFoto);
            infoBubble.addTab('Alamat', contentAlamat);
            infoBubble.open(map, marker);
         
        }));

        marker.category = contentId;
        marker.setVisible(false);

        kingMarkers.push(marker);
    }
  
    function show(category) {
      if(category == 9990 || category == 9991){
        page(category);
      }else{
        for (var i=0; i<listMarkers.length; i++) {
          var getMarker = listMarkers[i];
          if(getMarker[4] == category || category == 0) {
            kingMarkers[i].setVisible(true);
          }else{
            kingMarkers[i].setVisible(false);
          }
        }
      }
    }

    // Found Me
    function findMe(controlDiv, map) {
      controlDiv.style.padding = '7px';

      // Set CSS for the control border
      var controlUI = document.createElement('div');
      controlUI.style.backgroundColor = 'white';
      controlUI.style.cursor = 'pointer';
      controlUI.style.textAlign = 'center';
      controlUI.style.borderStyle = 'solid';
      controlUI.style.borderWidth = '1px';
      controlUI.style.borderColor = '#d0d0d0';
      controlUI.title = 'Find my current location';
      controlDiv.appendChild(controlUI);

      // Set CSS for the control interior
      var controlText = document.createElement('div');
      controlText.style.fontFamily = 'Arial,sans-serif';
      controlText.style.fontWeight = 'normal';
      controlText.style.fontSize = '10px';
      controlText.style.paddingTop = '1px';
      controlText.style.paddingRight = '7px';
      controlText.style.paddingBottom = '1px';
      controlText.style.paddingLeft = '7px';
      controlText.innerHTML = '<b>Find me</b>';
      controlUI.appendChild(controlText);

      // Geo location script
      google.maps.event.addDomListener(controlUI, 'click', function() {
        findMeAct(map);
      });
    }

    function findMeAct(map){
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
         
            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

            var image = {
              url: "gambar/icon/youre_here_icon.png",
              size: new google.maps.Size(32, 37),
              origin: new google.maps.Point(0,0),
              anchor: new google.maps.Point(0, 32)
            };

            var marker_findme = new google.maps.Marker({
                position: pos,
                map: map,
                icon: image,
                animation: google.maps.Animation.DROP,
                title: "Click to hide me"
            });

            google.maps.event.addListener(marker_findme, 'click', (function(){
              marker_findme.setVisible(false);
            }));

            var infowindow_findme = new google.maps.InfoWindow({
              map: map,
              position: pos,
              content: 'Youre here...'
            });


            map.setCenter(pos);


        }, function() {
          handleNoGeolocation(true);
        });
      } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
      }
    }

    function findGeolocation(direction_lat, direction_lng){
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {

            var latitude = position.coords.latitude;                    //users current
            var longitude = position.coords.longitude;                 //location
            var coords = new google.maps.LatLng(latitude, longitude); //Creates variable for map coordinates
            var desti = new google.maps.LatLng(direction_lat, direction_lng);
            var directionsService = new google.maps.DirectionsService();
            var mapOptions = //Sets map options
            {
             zoom: 15,  //Sets zoom level (0-21)
             center: coords, //zoom in on users location
             mapTypeControl: true, //allows you to select map type eg. map or satellite
             navigationControlOptions:
             {
               style: google.maps.NavigationControlStyle.SMALL //sets map controls size eg. zoom
             },
             mapTypeId: google.maps.MapTypeId.ROADMAP //sets type of map Options:ROADMAP, SATELLITE, HYBRID, TERRIAN
            };
            
            directionsDisplay.setMap(map);
            //directionsDisplay.setPanel(document.getElementById('panel'));

            var request = {
             origin: coords,
             destination: desti,
             travelMode: google.maps.DirectionsTravelMode.DRIVING
            };


               /* Start/Finish icons
               var icons = {
                start: new google.maps.MarkerImage(
                 // URL
                 'gambar/icon/start.png',
                 // (width,height)
                 new google.maps.Size( 44, 32 ),
                 // The origin point (x,y)
                 new google.maps.Point( 0, 0 ),
                 // The anchor point (x,y)
                 new google.maps.Point( 22, 32 )
                ),
                end: new google.maps.MarkerImage(
                 // URL
                 'gambar/icon/finish.png',
                 // (width,height)
                 new google.maps.Size( 44, 32 ),
                 // The origin point (x,y)
                 new google.maps.Point( 0, 0 ),
                 // The anchor point (x,y)
                 new google.maps.Point( 22, 32 )
                )
               };*/


            directionsService.route(request, function (response, status) {
             if (status == google.maps.DirectionsStatus.OK) {
               directionsDisplay.setDirections(response);

                var leg = response.routes[ 0 ].legs[ 0 ];
                makeMarkerDirection( coords, icons.start, "Start" );
                makeMarkerDirection( desti, icons.end, "End" );
             }
            });

        }, function() {
          handleNoGeolocation(true);
        });
      } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
      }   
    }

    function makeMarkerDirection( position, icon, title ) {
      new google.maps.Marker({
        position: position,
        map: map,
        icon: icon,
        title: title
      });
    }

    // Clear Maps
    function clearMaps(controlDiv, map) {
      controlDiv.style.padding = '7px';

      // Set CSS for the control border
      var controlUI = document.createElement('div');
      controlUI.style.backgroundColor = 'white';
      controlUI.style.cursor = 'pointer';
      controlUI.style.textAlign = 'center';
      controlUI.style.borderStyle = 'solid';
      controlUI.style.borderWidth = '1px';
      controlUI.style.borderColor = '#d0d0d0';
      controlUI.title = 'Clear/ reset maps';
      controlDiv.appendChild(controlUI);

      // Set CSS for the control interior
      var controlText = document.createElement('div');
      controlText.style.fontFamily = 'Arial,sans-serif';
      controlText.style.fontWeight = 'normal';
      controlText.style.fontSize = '10px';
      controlText.style.paddingTop = '1px';
      controlText.style.paddingRight = '7px';
      controlText.style.paddingBottom = '1px';
      controlText.style.paddingLeft = '7px';
      controlText.innerHTML = '<b>Clear map</b>';
      controlUI.appendChild(controlText);

      // Geo location script
      google.maps.event.addDomListener(controlUI, 'click', function() {
        directionsDisplay.setMap(null);
      });
    }

    function handleNoGeolocation(errorFlag) {
      if (errorFlag) {
        var content = 'Error: The Geolocation service failed.';
      } else {
        var content = 'Error: Your browser doesn\'t support geolocation.';
      }

      var options = {
        map: map,
        position: jogja,
        content: content
      };

      var infowindow = new google.maps.InfoWindow(options);
      map.setCenter(options.position);
    }
    

    google.maps.event.addDomListener(window, 'load', initialize); // Load Maps


    </script>

    <?php } ?>

  </head>
  <body>
   


