<?php 
if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }

if($id != ""){
	$baris = $pdb->Query("*", "obyek", "id_obyek = '$id'", "", "one");

	$input_id = 'value = "'.$id.'"';
	$input_judul = 'value = "'.$baris["judul"].'"';
	$input_lat = 'value = "'.$baris["lat"].'"';
	$input_lng = 'value = "'.$baris["lng"].'"';
	$input_desk = $baris["desk"];
	$input_alamat = 'value = "'.$baris["alamat"].'"';
	// $input_tips = $baris["tips"];
}else{
	$input_id = '';
	$input_judul = 'placeholder = "Judul"';
	$input_lat = 'placeholder = "Lat"';
	$input_lng = 'placeholder = "Lng"';
	$input_desk = '';
	$input_alamat = 'placeholder = "Alamat"';
	// $input_tips = 'placeholder = "Tips"';
}
?>

<div class="row">
	<div class="twelve columns">
		
		<input type="hidden" name="id" id="id" <?php echo $input_id; ?> />

		<div class="field" id="fkategori">
	  		<div class="picker">
	  		<select name="id_kategori" id="id_kategori">
	  			<?php
	  			$where_kategori = "";

	  			if($id != ""){
	  				$id_kategori = $baris["id_kategori"];

	  				$baris_kategori = $pdb->Query("*", "kategori", "id_kategori = '$id_kategori'", "", "one");

	  				$nama_kategori = $baris_kategori["nama"];
	  				echo "<option value='".$id_kategori."'>".$nama_kategori."</option>";

	  				$where_kategori = "id_kategori <> '$id_kategori'";
	  			}

	  			$get_kategori1 = $pdb->Query("*", "kategori", $where_kategori, "nama ASC", "all");
	  			while($baris_kategori1 = mysql_fetch_array($get_kategori1)){
	  				$id_kategori1 = $baris_kategori1["id_kategori"];
	  				$nama_kategori1 = $baris_kategori1["nama"];
	  				echo "<option value='".$id_kategori1."'>".$nama_kategori1."</option>";
	  			}
	  			?>	
		    </select>
			</div>
	    </div>


		<div class="field" id="fjudul">
		  <input class="input" type="text" id="judul" name"judul" <?php echo $input_judul; ?> />
		</div>
		<div class="field" id="flatlng">
		  <input class="narrow input" type="text" id="lat" name"lat" <?php echo $input_lat; ?> />
		  <input class="narrow input" type="text" id="lng" name"lng" <?php echo $input_lng; ?> />
		</div>
		<div class="field" id="fdesk">
		  <textarea class="xxwide text input textarea" placeholder="Deskripsi" id="desk" name="desk"><?php echo $input_desk; ?></textarea>	
		</div>
		<div class="field" id="falamat">
		  <input class="input" type="text" id="alamat" name"alamat" <?php echo $input_alamat; ?> />
		</div>

		<!-- <div class="field" id="ftips">
			<textarea class="xxwide text input textarea" placeholder="Tips" id="tips" name="tips"><?php echo $input_tips; ?></textarea>	
		</div>	-->	

	</div>
</div>

<div class="row">
	<div class="twelve columns right">
		<div class="medium primary btn icon-right entypo icon-check" style="cursor:pointer;" onclick="saveLetak()"><a>Simpan Data</a></div>
		<?php if($id != ""){ echo '<div class="cus-btn medium info btn icon-left entypo icon-cancel" onclick="deleteAct()" style="cursor:pointer;"><a>Hapus</a></div>'; } ?>

	</div>
</div>

<div class="row">&nbsp;</div>

<script type="text/javascript">
	
	function saveLetak(){
		
		var id	= $("#id").val();

		var id_kategori	= $("#id_kategori").val();
		var judul	= $("#judul").val();
		var lat 	= $("#lat").val();
		var lng 	= $("#lng").val();
		var desk 	= $("#desk").val();
		var alamat 	= $("#alamat").val();
		// var tips 	= $("#tips").val();
		
		if(judul != ""){ $('#fjudul').removeClass('warning'); }
		if(lat != ""){ $('#flat').removeClass('warning'); }
		if(lng != ""){ $('#flng').removeClass('warning'); }

		if(judul == ""){ $('#fjudul').addClass('warning');
		}else if(lat == ""){ $('#flat').addClass('warning');
		}else if(lng == ""){ $('#flng').addClass('warning');
		}else{

			var kiriman = "&id="+id+"&id_kategori="+id_kategori+"&judul="+judul+"&lat="+lat+"&lng="+lng+"&desk="+desk+"&alamat="+alamat;	

			$.ajax({
				type: 	"GET",
				url: 	"../mod/function.php?type=save_obyek" + kiriman,
				success : function(result){
					
					var result = eval('('+result+')');
					if(result.sukses){
						location = ".?hal=obyek";
					}else{
						alert("Maaf, ada kesalahan dalam pemrosesan data");
					}

				}
			});

		}
	}

	function deleteConf(){
		//Tampilkan pesan
		notif("Anda Yakin? <div class='small btn default del-item yesno_btn' onclick='deleteAct()' style='cursor:pointer;'><a>Yes</a></div> <div class='small btn default del-item yesno_btn' style='cursor:pointer;' onclick='closeNotif()'><a>No</a></div>", "");
	}

	function deleteAct(){
		if (confirm('Anda Yakin?')) {
		    var id 	= $("#id").val();
			$.ajax({
				type: 	"GET",	
				url: 	"../mod/function.php?type=del_obyek&id="+id,
				success : function(result){
					
					var result = eval('('+result+')');
					if(result.sukses){
						location = ".?hal=obyek";
					}else{
						alert("Maaf, ada kesalahan dalam pemrosesan data");
					}

				}
			});
		} else {
		    // Do nothing!
		}
		
	}

</script>