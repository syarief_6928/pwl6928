<?php 
if(isset($_GET["id"])){ $id = $psys->anti_injection($_GET["id"]); }else{ $id = ""; }

if($id != ""){
	$baris = $pdb->Query("*", "kategori", "id_kategori = '$id'", "", "one");

	$input_id = 'value = "'.$id.'"';
	$input_nama = 'value = "'.$baris["nama"].'"';
	$input_penanda = 'value = "'.$baris["penanda"].'"';
}else{
	$input_id = '';
	$input_nama = 'placeholder = "Kategori"';
	$input_penanda = 'placeholder = "Penanda"';

}
?>

<div class="row">
	<div class="twelve columns">
		
		<input type="hidden" name="id" id="id" <?php echo $input_id; ?> />

		<div class="field" id="fnama">
		  <input class="input" type="text" id="nama" name"nama" <?php echo $input_nama; ?> />
		</div>

	</div>
</div>

<div class="row">
	<div class="twelve columns right">
		<div class="medium primary btn icon-right entypo icon-check" style="cursor:pointer;" onclick="saveKategori()"><a>Simpan Data</a></div>
		<?php if($id != ""){ echo '<div class="cus-btn medium info btn icon-left entypo icon-cancel" onclick="deleteAct()" style="cursor:pointer;"><a>Hapus</a></div>'; } ?>
		<div class="cus-btn medium info btn icon-left entypo icon-reply" style="cursor:pointer;" onclick="history.go(-1)"><a>Batal</a></div>
	</div>
</div>

<div class="row">&nbsp;</div>

<script type="text/javascript">
	
	function saveKategori(){
		
		var id	= $("#id").val();

		var nama	= $("#nama").val();

		if(nama != ""){ $('#fnama').removeClass('warning'); }

		if(nama == ""){ $('#fnama').addClass('warning');
		}else{

			var kiriman = "&id="+id+"&nama="+nama;	

			$.ajax({
				type: 	"GET",
				url: 	"../mod/function.php?type=save_kategori" + kiriman,
				success : function(result){
					
					var result = eval('('+result+')');
					if(result.sukses){
						location = ".?hal=kategori";
					}else{
						alert("Maaf, ada kesalahan dalam pemrosesan data");
					}

				}
			});

		}
	}

	function deleteConf(){
		//Tampilkan pesan
		notif("Anda Yakin? <div class='small btn default del-item yesno_btn' onclick='deleteAct()' style='cursor:pointer;'><a>Yes</a></div> <div class='small btn default del-item yesno_btn' style='cursor:pointer;' onclick='closeNotif()'><a>No</a></div>", "");
	}

	function deleteAct(){
		if (confirm('Anda Yakin?')) {
		    var id 	= $("#id").val();
			$.ajax({
				type: 	"GET",	
				url: 	"../mod/function.php?type=del_kategori&id="+id,
				success : function(result){
					
					var result = eval('('+result+')');
					if(result.sukses){
						location = ".?hal=obyek";
					}else{
						alert("Maaf, ada kesalahan dalam pemrosesan data");
					}

				}
			});
		} else {
		    // Do nothing!
		}
		
	}

</script>