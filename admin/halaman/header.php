<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PhotoTrip - Sistem Informasi Geografis Untuk Pemetaan Lokasi Spot Foto di Yogyakarta</title>
		<link rel="stylesheet" type="text/css" href="style/css/gumby.css">
		<link rel="stylesheet" type="text/css" href="style/css/style.css">
		<link rel="stylesheet" type="text/css" href="style/css/redactor.css">

		<script src="../style/js/jquery-1.11.1.min.js"></script>
		<script src="style/js/jquery.fileuploadmulti.min.js"></script>
		<script src="style/js/redactor.min.js"></script>

</head>
<body <?php if($hal == "login"){ echo "id='body_login'"; } ?>>
<?php if($hal != "login"){ ?>	
	<div class="header_real">
		<div class="row">
			<div class="six columns white">
				<a href=".?hal=dashboard" style="color:#fff;"><b>PhotoTrip</b></a>
			</div>
			<div class="six columns right">
				<a href=".?hal=halaman" title="Halaman"><i class="icon-docs white" ></i></a>
				<a href=".?hal=obyek" title="obyek"><i class="icon-location white"></i></a>
				<a href=".?hal=user" title="User"><i class="icon-users white" ></i></a>
				<i class="icon-flow-line white"></i>
				<a href=".?hal=dashboard" title="Dashboard"><i class="icon-home white"></i></a>
				<a href=".?hal=logout" title="Log Out"><i class="icon-logout white"></i></a>
			</div>
      </div>
	</div>	
	<div class="row">&nbsp;</div>
<?php } ?>	